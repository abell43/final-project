//est1.cpp


#include <cassert>
#include <iostream>
#include "Bishop.h"
#include "King.h"
#include "Queen.h"
#include "Knight.h"
#include "Rook.h"
#include "CreatePiece.h"
#include "Board.h"
#include "Chess.h"
#include "Terminal.h"

using std::pair;
using std::make_pair;

int main() {

  Chess chess;

  chess.board().display();

  assert(chess.make_move(make_pair('B', '2'), make_pair('B', '4')));

  Piece* k = create_piece('K');
  assert(k->legal_move_shape(make_pair('A','2'),make_pair('A','3')));
  assert(!(k->legal_move_shape(make_pair('A','2'),make_pair('A','4'))));
  assert(k->legal_move_shape(make_pair('A','2'),make_pair('B','3')));
  Piece* b = create_piece('B');
  assert(b->legal_move_shape(make_pair('C','2'),make_pair('D','3')));
  assert(b->legal_move_shape(make_pair('C','2'),make_pair('A','4')));
  assert(!(b->legal_move_shape(make_pair('C','2'),make_pair('C','3'))));
  Piece* q = create_piece('Q');
  assert(q->legal_move_shape(make_pair('A','1'),make_pair('C','3')));
  assert(q->legal_move_shape(make_pair('A','1'),make_pair('E','1')));
  assert(q->legal_move_shape(make_pair('A','1'),make_pair('A','5')));
  assert(!(q->legal_move_shape(make_pair('A','1'),make_pair('B','3'))));
  Piece* kn = create_piece('N');
  assert(kn->legal_move_shape(make_pair('C','3'),make_pair('B','5')));
  assert(kn->legal_move_shape(make_pair('C','3'),make_pair('A','4')));
  assert(kn->legal_move_shape(make_pair('C','3'),make_pair('B','1')));
  assert(kn->legal_move_shape(make_pair('C','3'),make_pair('D','1')));
  assert(kn->legal_move_shape(make_pair('C','3'),make_pair('E','4')));

  assert(!(kn->legal_move_shape(make_pair('C','3'),make_pair('E','3'))));
  assert(!(kn->legal_move_shape(make_pair('C','3'),make_pair('D','6'))));

  Piece* bp = create_piece('p');
  Piece* wp = create_piece('P');
  assert(wp->legal_move_shape(make_pair('C','2'),make_pair('C','4')));
  assert(wp->legal_move_shape(make_pair('C','4'),make_pair('C','5')));
  assert(wp->legal_move_shape(make_pair('C','2'),make_pair('C','3')));
  assert(bp->legal_move_shape(make_pair('C','7'),make_pair('C','5')));
  assert(bp->legal_move_shape(make_pair('C','5'),make_pair('C','4')));
  assert(!(bp->legal_move_shape(make_pair('C','7'),make_pair('D','7'))));
  assert(!(wp->legal_move_shape(make_pair('C','3'),make_pair('C','5'))));
  assert(!(wp->legal_move_shape(make_pair('C','6'),make_pair('C','5'))));
  assert(!(bp->legal_move_shape(make_pair('C','6'),make_pair('C','7'))));
  std::cout << "all assertions passed" << std::endl;
}
